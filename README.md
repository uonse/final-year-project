# README #

The Project

### Installation Requirements ###

* [NodeJS](https://nodejs.org/)

### Install ###

* Open a terminal/commandline to project directory. Then run the following to install relevant packages.

	npm install
	
	bower install
	
### Running

Running the application is initiated with the following command:
	
	gulp
	
* Take notice of the terminal output for relevant JSHint errors, etc.
* Goto the following URL in your browser to view the App: 

	http://localhost:7575/
	
